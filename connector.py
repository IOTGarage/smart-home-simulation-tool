from selenium import webdriver
from selenium.webdriver.common.by import By
from os import system, path
import _thread as thread
import appscript
import subprocess
import json

options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches", ["enable-automation"])
options.add_experimental_option('useAutomationExtension', False)
driver = webdriver.Chrome(executable_path='./drivers/chromedriver', options=options)
print(path.abspath("./web/ui.html"))
driver.get("file://"+path.abspath("./web/ui.html"))
Area = ""

# tracking user activity in browser by looking at html content.
def userActivity():
	global Area
	while True:
		if driver.find_element(By.ID, 'connectMessage').text.__contains__("Connecting"):
			clientsFile = open("clients.json", "w")
			#clientsFile.write(json.dumps({eventName: {"topic": topic, "payload": payload, "eventTime": eventTime, "eventDate": eventDate, "eventDays": eventDays, "eventType": eventType}}))
			clientsFile.write(driver.find_element(By.ID, 'clientsJsonBox').text)
			clientsFile.close()


			mqttHost = driver.find_element(By.ID, 'mqttHost').get_attribute("value")
			mqttUsername = driver.find_element(By.ID, 'mqttUsername').get_attribute("value")
			mqttPassword = driver.find_element(By.ID, 'mqttPassword').get_attribute("value")
			mqttPort = driver.find_element(By.ID, 'mqttPort').get_attribute("value")

			clientsFile = open("settings.json", "w")
			clientsFile.write(driver.find_element(By.ID, 'mqttJsonBox').text)
			#clientsFile.write(json.dumps({eventName: {"topic": topic, "payload": payload, "eventTime": eventTime, "eventDate": eventDate, "eventDays": eventDays, "eventType": eventType}}))
			clientsFile.close()
			
			output = subprocess.Popen( "pwd", stdout=subprocess.PIPE ).communicate()[0]
			output = str(output)
			output = output[2:-3]
			appscript.app('Terminal').do_script('cd '+output+' && python3 main.py')
			break




# creating threads for parallel processing.
thread.start_new_thread(userActivity, ())

while True:
	continue

driver.close()