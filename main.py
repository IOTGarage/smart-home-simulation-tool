from mqtt import MqttConnector
from os import system
import subprocess
import appscript
import _thread as thread
import json

from datetime import datetime

def showMenu():
	while (True):
		system("clear")
		print("1) Show Devices")
		option = input("Choose an option: ")
		system("clear")

		if option == "1":
			mqttClients.print_devices()

		input("Press Enter to Continue...")
		system("clear")

# for initializing MqttConnector Class
with open('settings.json') as json_file:
	data = json.load(json_file)
	global mqttClients
	mqttClients = MqttConnector(data['ip'], data['port'], data['username'], data['password'])

# for publishing payloads
with open('clients.json') as json_file:
	data = json.load(json_file)
	for clientName in data.keys():
		mqttClients.add_client(clientName)
		mqttClients.publish(clientName, data[clientName]['topic'], data[clientName]['payload'], data[clientName]['eventType'], data[clientName]['eventTime'], data[clientName]['eventDays'], data[clientName]['eventDate'])


output = subprocess.Popen( "pwd", stdout=subprocess.PIPE ).communicate()[0]
output = str(output)
output = output[2:-3]
thread.start_new_thread(showMenu, ())
appscript.app('Terminal').do_script('cd '+output+' && python3 logdisplay.py')


while (True):
	pass