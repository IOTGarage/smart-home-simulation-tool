import paho.mqtt.client as mqtt
from tabulate import tabulate
import _thread as thread
import datetime
import time

class MqttConnector:

	# for keeping track of name of each device
	__clientNames = {}
	# for keeping track of result code of each device
	__clientResultCodes = {}
	# for keeping track of publication of each device
	__clientPublishings = {}
	# day of start
	__clientPublishingDate = {}

	def __init__(self, ip, port, username, password):
		self.ip = ip
		self.port = port
		self.usr = username
		self.pwd = password
		self.log = open("log.txt", "w")

	def add_client(self, clientName):
		client = mqtt.Client(clientName)
		client.username_pw_set(username = self.usr, password = self.pwd)
		client.on_connect = self.on_connect
		client.connect(self.ip, int(self.port), 60)
		self.__clientNames[client] = clientName
		thread.start_new_thread(client.loop_forever, ())

	# check if client is connected by checking its result code
	def isClientConnected(self, clientName):
		clientObj = self.__fromNameToObj(clientName)
		if clientObj in self.__clientResultCodes.keys():
			return self.__clientResultCodes[clientObj]
		else:
			return 1

	def on_connect(self, client, userdata, flags, rc):
		self.__clientResultCodes[client] = rc
	

	# helper function
	def __fromNameToObj(self, clientName):
		clientObj = None
		for k, v in self.__clientNames.items():
			if v == clientName:
				clientObj = k
				break

		return clientObj

	def publish(self, clientName, topic, payload, eventType, eventTime, eventDays, eventDate):
		
		# get client object from user provided clientName
		clientObj = self.__fromNameToObj(clientName)

		# if client is none, then we don't publish
		if clientObj is None:
			return 1

		# converting eventDate from string format to date format
		eventDate = datetime.datetime.strptime(eventDate, "%Y-%m-%d");
		
		# append publications in __clientPublishings
		if clientObj not in self.__clientPublishings.keys():
			self.__clientPublishings[clientObj] = [[topic, payload, eventType, eventTime, eventDays, eventDate]]
		else:
			self.__clientPublishings[clientObj].append([topic, payload, eventType, eventTime, eventDays, eventDate])

		if (int(eventDays) >= 0):
			self.__clientPublishingDate[clientObj] = datetime.date.today()
			self.__clientPublishingDate[clientObj] += datetime.timedelta(days=int(eventDays))
		else:
			self.__clientPublishingDate[clientObj] = None

		# special cases
		if eventType.lower() == "periodic":
			thread.start_new_thread(self.__publish_periodic, (clientObj, clientName, topic, payload, eventType, eventTime, eventDays, eventDate))
		else:
			thread.start_new_thread(self.__publish_once, (clientObj, clientName, topic, payload, eventType, eventTime, eventDays, eventDate))
			
		return 0

	# publishes after every eventTime
	def __publish_periodic(self, clientObj, clientName, topic, payload, eventType, eventTime, eventDays, eventDate):
		clientObj.publish(topic, payload)
		timesplit = eventTime.split(":")
		timewait = int(timesplit[0])*3600+int(timesplit[1])*60+int(timesplit[2])
		while True:
			if datetime.datetime.now() < eventDate:
				continue
			if self.__clientPublishingDate[clientObj] is not None and datetime.date.today() >= self.__clientPublishingDate[clientObj]:
				break
			time.sleep(timewait)
			clientObj.publish(topic, payload)
			self.log.write("["+datetime.datetime.now().strftime("%H:%M:%S")+"] '"+ clientName+"' published a payload in topic '"+ topic+ "' ("+eventType+")\n")
			self.log.flush()

	# publishes at eventTime once
	def __publish_once(self, clientObj, clientName, topic, payload, eventType, eventTime, eventDays, eventDate):
		while True:
			if datetime.datetime.now() < eventDate:
				continue
			if self.__clientPublishingDate[clientObj] is not None and datetime.date.today() >= self.__clientPublishingDate[clientObj]:
				break
			if datetime.datetime.now().strftime("%H:%M:%S") == eventTime:
				clientObj.publish(topic, payload)
				self.log.write("["+datetime.datetime.now().strftime("%H:%M:%S")+"] '"+ clientName+"' published a payload in topic '"+ topic+ "' ("+eventType+")\n")
				self.log.flush()
				break

	def print_devices(self):
		print("Client Devices: ")
		for obj, name in self.__clientNames.items():
			print()
			print(name, end=" ")
			if obj in self.__clientResultCodes.keys() and self.__clientResultCodes[obj] == 0:
				print("[Connected]")
			else:
				print("[Not Connected]")
			print()
			infoTable = []
			if obj in self.__clientPublishings.keys():
				for info in self.__clientPublishings[obj]:
					infoTable.append(info)
			print(tabulate(infoTable, ['Topic', 'Payload', 'EventType', 'EventTime', 'EventDays', 'EventDate']))
			print()
